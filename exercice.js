class User {
  constructor(name, email) {
    this.name = name;
    this.email = email;
    this.id = User.generateId();
  }

  toString() {
    //console.log(this.name + " " + this.email + " " + this.id);
    console.log(`${this.name}    ${this.email}    ${this.id}`)
  }
//uuid method
  static generateId() {
    return Date.now();
  }

}
//create new users
getEmailDomain = async() => {
  //fetch the data
  //const fetch = await import("node-fetch");
  const res = await fetch(
    "https://jsonplaceholder.typicode.com/users"
  );
  const jsonData = await res.json();

  const emailTable = []
  jsonData.map( item => {
    emailTable.push(item.email) 
  })

  emailDomain = []
  emailTable.map(item => {
      const emailSplit = item.split('@')
      emailDomain.push(
          emailSplit[1]
      )
  } )  
  console.log(emailDomain)
}

const p1 = new User("Leanne Graham", "Sincere@april.biz");
const p2 = new User("Chelsey Dietrich", "Lucio_Hettinger@annie.ca");
const p3 = new User("Clementine Bauch", "Nathan@yesenia.net");

//adding users to the arra
const arr = [p1, p2, p3];

p1.toString()
//filtering users by first letter 
const filtered_users = arr.filter((value) => {
  return value.name[0] == "C";
});

//mapping the filtered array
mapped = filtered_users.map((item) => {
  return item.name;
});

//printing the mapped array
mapped.forEach((element) => {
  console.log(element);
});

//getting the email domain of filtered users
for (item of filtered_users) {
  //item.getEmailDomain();
}

getEmailDomain()